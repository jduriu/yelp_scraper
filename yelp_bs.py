# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
import requests


def get_context():
    headers = {'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/601.3.9 (KHTML, like Gecko) Version/9.0.2 Safari/601.3.9'}
    # this is a User Agent string (mimicing human resquest and lets the site know you are scraping?)
    url = 'https://www.yelp.com/search?cflt=restaurants&find_loc=San%20Jose%2C%20CA%2095125'

    response=requests.get(url,headers=headers)

    # print(response.content)

    soup = BeautifulSoup(response.content,'lxml')

    # print(soup.select('.css-1egxyvc')[0].get_text())
    # select function
    # find function
    recommendations = []
    for item in soup.select('[class*=container]'):
        try:
            if item.find('h3'):
                heading = item.find('h3').get_text()
                recommendations.append(heading)
        except Exception as e:
            raise e
            print('')

    final_recs = []
    for rec in recommendations:
        if u"\xa0" in rec:
            split = rec.split(u"\xa0")
            final_recs.append(split[1])


    return final_recs


print(get_context())
