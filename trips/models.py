from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin


class Trip(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    image = models.URLField(null=True, blank=True)
    url_name = models.URLField(null=True, blank=True)
    arrival_date = models.DateTimeField()
    departure_date = models.DateTimeField()
    guests = models.ManyToManyField(User, related_name="trips")

    def __str__(self):
        return self.name
