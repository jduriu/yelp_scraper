from django.shortcuts import render
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from trips.models import Trip

from bs4 import BeautifulSoup
import requests


# Create your views here.
class TripListView(LoginRequiredMixin, ListView):
    model = Trip
    template_name = "trips/list.html"

    def get_queryset(self):
        return Trip.objects.filter(guests=self.request.user)


class TripDetailView(LoginRequiredMixin, DetailView):
    model = Trip
    template_name = "trips/detail.html"

    def get_context_data(self, *args, **kwargs):
        headers = {'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/601.3.9 (KHTML, like Gecko) Version/9.0.2 Safari/601.3.9'}
        url = self.object.url_name
        response=requests.get(url,headers=headers)
        soup = BeautifulSoup(response.content,'lxml')

        recommendations = []
        for item in soup.select('[class*=container]'):
            try:
                if item.find('h3'):
                    recommendations.append(item.find('h3').get_text())
            except Exception as e:
                raise e

        final_recs = []
        for rec in recommendations:
            if u"\xa0" in rec:
                split = rec.split(u"\xa0")
                final_recs.append(split[1])

        context = super(TripDetailView, self).get_context_data(*args, **kwargs)
        context['recommendations'] = final_recs
        return context

class TripCreateView(LoginRequiredMixin, CreateView):
    model = Trip
    fields = "__all__"
    template_name = "trips/create.html"

    def get_success_url(self):
        return reverse_lazy("trip_detail", args=[self.object.id])


class TripUpdateView(LoginRequiredMixin, UpdateView):
    model = Trip
    fields = "__all__"
    template_name = "trips/update.html"
    success_url = reverse_lazy("trip_list")


class TripDeleteView(LoginRequiredMixin, DeleteView):
    model = Trip
    template_name = "trips/delete.html"
    success_url = reverse_lazy("trip_list")


def home_view(request):
    return render(request, 'home.html')
