from django.urls import path
from trips.views import (
    TripListView,
    TripDetailView,
    TripCreateView,
    TripUpdateView,
    TripDeleteView,
)

urlpatterns = [
    path("", TripListView.as_view(), name="trip_list"),
    path("<int:pk>/", TripDetailView.as_view(), name="trip_detail"),
    path("create/", TripCreateView.as_view(), name="create_trip"),
    path("<int:pk>/update/", TripUpdateView.as_view(), name="update_trip"),
    path("<int:pk>/delete", TripDeleteView.as_view(), name="delete_trip"),
]
