from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Event(models.Model):
    name = models.CharField(max_length=100)
    author = models.ForeignKey(User, related_name="events", on_delete=models.CASCADE)
    image = models.URLField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    location = models.CharField(max_length=100)
    start_time = models.DateTimeField()
    duration = models.DurationField()
    trip = models.ForeignKey("trips.Trip", related_name="events", on_delete=models.CASCADE)
    type = models.ForeignKey("Type", on_delete=models.PROTECT, blank=True)

    def __str__(self):
        return self.name


class Type(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name
