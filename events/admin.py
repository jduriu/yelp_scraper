from django.contrib import admin
from events.models import Event, Type
# Register your models here.

class EventAdmin(admin.ModelAdmin):
    pass

class TypeAdmin(admin.ModelAdmin):
    pass

admin.site.register(Event, EventAdmin)
admin.site.register(Type, TypeAdmin)
